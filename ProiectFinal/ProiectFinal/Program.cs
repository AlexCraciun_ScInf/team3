﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProiectFinal.Interfaces;
using ProiectFinalCore;


namespace ProiectFinal
{
    class Program
    {
        static ICatalog animalCatalog = new Catalog();
        static ISelect selected = new SelectAnimal(animalCatalog);
        static void Main(string[] args)
        {
            Console.WriteLine("This is our animal collection!");
            Console.WriteLine(animalCatalog.Present());

            while(true)
            {

                Console.WriteLine("Do you want to select an animal? Yes/No");
                string input = Console.ReadLine();

                if (input == "Yes")
                {
                    Console.WriteLine("Insert the animal's name:");
                    string input2 = Console.ReadLine();

                    bool s = selected.SelectAnimalByName(input2);
                    if (s)
                    {
                        Console.WriteLine("Animal selected");
                        string output = selected.GetBasket();

                        //Console.WriteLine(animalCatalog.Present());
                        //Console.WriteLine(output);

                        Console.WriteLine("Do you want to evolve/de-evolve the animal? Evolve/De-evolve/No.");
                        string input4 = Console.ReadLine();
                        if (input4 == "Evolve")
                        {

                            animalCatalog.evolve(input2);

                            Console.WriteLine("Unselect animals? Yes/No");
                            string input3 = Console.ReadLine();
                            if (input3 == "Yes")
                            {
                                selected.CheckoutBasket();
                            }
                            else
                            {
                                Console.WriteLine("Animals are in the list");
                            }

                            output = selected.GetBasket();

                            Console.WriteLine(animalCatalog.Present());
                            Console.WriteLine(output);
                        }
                        else if (input4 == "De-evolve")
                        {
                            animalCatalog.de_evolve(input2);

                            Console.WriteLine("Unselect animals? Yes/No");
                            string input3 = Console.ReadLine();
                            if (input3 == "Yes")
                            {
                                selected.CheckoutBasket();
                            }
                            else
                            {
                                Console.WriteLine("Animals are in the list");
                            }

                            output = selected.GetBasket();

                            Console.WriteLine(animalCatalog.Present());
                            Console.WriteLine(output);
                        }
                        else 
                        {
                            Console.WriteLine("No evolution or de-evolution.");
                            Console.WriteLine(animalCatalog.Present());
                            Console.WriteLine(output);
                        }




                    }
                    else
                    {
                        Console.WriteLine("Couldn't choose.");
                    }

                }
                if (input == "No")
                {
                    Console.WriteLine("No animal selected.");
                    break;
                }

            }


            Console.ReadLine();
        }
    }
}
