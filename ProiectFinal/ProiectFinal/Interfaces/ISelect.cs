﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectFinal.Interfaces
{
    public interface ISelect
    {
        bool SelectAnimalByName(string name);
        string GetBasket();
        bool CheckoutBasket();
    }
}
