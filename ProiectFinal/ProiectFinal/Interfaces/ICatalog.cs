﻿using ProiectFinalCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectFinal.Interfaces
{
    public interface ICatalog
    {
        void PopulateCatalog();

        string Present();
        Animal GetAnimal(string name);

        void evolve(string s);

        void de_evolve(string s);

      //  int SearchAfterName(string y);
    }
}
