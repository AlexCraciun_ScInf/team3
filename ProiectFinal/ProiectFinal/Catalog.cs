﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invertebrate.Arthropods;
using Invertebrate.Insects;
using Invertebrate.Mollusks;
using Invertebrate.Sponges;
using Invertebrate.Worms;
using Vertebrate.Fish;
using ProiectFinal.Interfaces;
using ProiectFinalCore.Interfaces;
using ProiectFinalCore;
using Vertebrate.Bird;
using Vertebrate.Mammals;
using Vertebrate.Reptiles;
using Vertebrate;

namespace ProiectFinal 
{
    public class Catalog : ICatalog, IEvolution, IDeEvolution
    {
        List<Animal> animals = new List<Animal>();
        public Catalog()
        {
            PopulateCatalog();
        }
        public void PopulateCatalog()
        {
            var animal1 = new Arthropods("Artropod1", 4, 5, "Europa", false, 4 );
            var animal2 = new Crustacea("Crustaceu1", 2, 10, "Asia", false, 4);
            var animal3 = new Myriapod("Myriapod 1", 1, 8, "Asia", false, 10);
            var animal4 = new Shrimp("Crevete 1", 2, 4, "Ocean", false, 20);

            var animal5 = new Insects("Insecta1", 1, 1, "Europa", "Cricri");
            var animal6 = new FlyingInsects("Insecta2", 1, 2, "Asia, Europa", "Bzzz", 6);

            var animal7 = new Mollusks("Molusca 1", 3,4,"Europa Centrala", 0);
            var animal8 = new Clam("Clam 1 ", 4, 7, "Asia", 0, true, "white");
            var animal9 = new Octopus("Octopus 2", 5, 10, "Oceanul Pacific", 8, false, true);

            animals.Add(animal1);
            animals.Add(animal2);
            animals.Add(animal3);
            animals.Add(animal4);
            animals.Add(animal5);
            animals.Add(animal6);
            animals.Add(animal7);
            animals.Add(animal8);
            animals.Add(animal9);

        }
        public string Present()
        {
            var builder = new StringBuilder();
            foreach (var x in animals)
            {
                builder.AppendLine(x.Present());
            }
            return builder.ToString();
        }

        //public string SearchAfterName(string y)
        //{
        //    for (int i = 0; i < animals.Count; i++)
        //    {
        //        if (animals[i].Name == y)
        //        {
        //            return "S-a gasit animalul!";
        //        }

        //    }
        //    return "Nu s-a gasit animalul!";
        //}
        public Animal GetAnimal(string name)
        {
            return animals.FirstOrDefault(p => p.Name == name);
        }

        public void de_evolve(string deEvolution)
        { 
            int i = this.GetPosition(deEvolution);
            Type currentElement = animals[i].GetType();
            if (currentElement != typeof(Animal))
            {
                Animal x = new Animal(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }

        }

        public void evolve(string Evolution)
        {
            int i = this.GetPosition(Evolution);
           

            Type currentElement = animals[i].GetType();
            if (currentElement == typeof(Birds))
            {
                Animal x = new Parrots(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }
            else if (currentElement == typeof(Arthropods) || currentElement == typeof(Crustacea) || currentElement == typeof(Myriapod))
            {
                Animal x = new Shrimp(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area, false, 7);
                animals[i] = x;
            }
            else if (currentElement == typeof(Insects))
            {
                Animal x = new FlyingInsects(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area, "Bzz", 4);
                animals[i] = x;
            }
            else if (currentElement == typeof(Mollusks))
            {
                Animal x = new Octopus(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area, 4, false, false);
                animals[i] = x;
            }
            else if (currentElement == typeof(Sponges))
            {
                Animal x = new ColonySponge(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }
            else if (currentElement == typeof(Worms))
            {
                Animal x = new FlatWorms(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area, "little", "black");
                animals[i] = x;
            }
            else if (currentElement == typeof(Fish) || currentElement == typeof(JawlessFish) || currentElement == typeof(CartilaginousFish))
            {

                Animal x = new BonyFish(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }
            else if (currentElement == typeof(Mammals))
            {
                Animal x = new Carnivore(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }
            else if (currentElement == typeof(Reptiles))
            {
                Animal x = new Snake(animals[i].Name, animals[i].Age, animals[i].EstimatedLifeTime, animals[i].Area);
                animals[i] = x;
            }
            else
            {
                Console.WriteLine("The animal cannot evolve!");
            }

        }

        public int GetPosition(string y)
        {
            for (int i = 0; i < animals.Count; i++)
            {
                if (animals[i].Name == y)
                {
                    return i;
                }
            }
            return 0;
        }




    }
}
