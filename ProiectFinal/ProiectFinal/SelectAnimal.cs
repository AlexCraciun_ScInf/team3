﻿using ProiectFinal.Interfaces;
using ProiectFinalCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectFinal
{
    public class SelectAnimal : ISelect
    {
        private readonly ICatalog animalCatalog;
        private List<Animal> Basket = new List<Animal>();

        public SelectAnimal(ICatalog catalog)
        {
            this.animalCatalog = catalog;
        }

        public string GetBasket()
        {
            var builder = new StringBuilder();
            foreach (var anim in Basket)
            {
                builder.AppendLine(anim.Present());
            }
            return builder.ToString();
        }
        public bool SelectAnimalByName(string name)
        {
            var animalName = animalCatalog.GetAnimal(name);

            if (animalName == null) return false;
            Basket.Add(animalName);
            return true;
        }
        public bool CheckoutBasket()
        {
            Basket.Clear();
            return true;
        }
    }
}
