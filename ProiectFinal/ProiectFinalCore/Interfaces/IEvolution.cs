﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectFinalCore.Interfaces
{
    public interface IEvolution
    {
        void evolve(string y);
    }
}
