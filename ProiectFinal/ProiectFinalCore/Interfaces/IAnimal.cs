﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectFinalCore.Interfaces
{
    interface IAnimal
    {
        string Present();
        string HowYoungIsTheAnimal();
    }
}
