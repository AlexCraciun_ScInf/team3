﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProiectFinalCore.Interfaces;
namespace ProiectFinalCore
{
    public class Animal : Eater, IAnimal
    {
       // private static int baseId = 1;
      //  public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int EstimatedLifeTime { get; set; }
        public string Area { get; set; } //zona in care se poate gasi
        public Animal() { }
        public Animal(string name,int age, int estimatedLifeTime, string area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
           // this.Id = baseId;
          //  baseId++;
        }
        public virtual string Present()
        {
            return "I' m a " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in: " + Area;
        }
        public virtual string HowYoungIsTheAnimal()
        {
            if (Age < EstimatedLifeTime / 3)
                return " The animal is young ";
            else if (Age > (EstimatedLifeTime / 3) && Age < (EstimatedLifeTime * 2 / 3))
                return " The animal is an adult ";
            else 
                return " The animal is old ";
        }
    }
}
