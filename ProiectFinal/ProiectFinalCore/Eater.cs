﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProiectFinalCore.Interfaces;

namespace ProiectFinalCore
{
    public class Eater  :IEater
    {
        public string Type { get; set; }
        public Eater() { }
        public Eater(string type)
        {
            this.Type = type;
        }
        public string Eating()
        {
            return "This type of animal is a: " + Type;
        }
    }
}
