﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProiectFinalCore;
namespace Invertebrate
{
    public class Invertebrate : Animal
    {
        public Invertebrate() { }
        public Invertebrate(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area) { }
        public override string Present()
        {
            return "I' m a " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
                " and i don't have the vertebral column";

        }
    }
}
