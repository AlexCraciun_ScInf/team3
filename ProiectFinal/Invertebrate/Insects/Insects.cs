﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Insects
{
    public class Insects : Invertebrate
    {
        public bool Fly { get; set; }
        public int NumberOfLegs { get; set; }
        public string Sound { get; set; }
        public Insects() { }
        public Insects(string name, int age, int estimatedLifeTime, string area, string sound) : base(name, age, estimatedLifeTime, area)
        {
            this.Sound = sound;
        }
        public override string Present()
        {
            return "I' m an insect with the name: " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area

               + " and i don't have the vertebral column";

        }
        public override string HowYoungIsTheAnimal()
        {
            return "The insects live a really short period of time.Some of them just a number of hours, so we can't classify them exactly.";
        }
    }
}
