﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Insects
{
    public class FlyingInsects : Insects
    {
        public int NumberOfWings { get; set; }
        public FlyingInsects() { }
        public FlyingInsects(string name, int age, int estimatedLifeTime, string area, string sound, int numberOfWings) : base(name, age, estimatedLifeTime, area, sound)
        {
            this.NumberOfWings = numberOfWings;
        }
        public override string Present()
        {
            return "I' m a flying insect with the name: " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area

               + " and i don't have the vertebral column";

        }
    }
}
