﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Arthropods
{
    public class Arthropods : Invertebrate
    {
        public bool Fly { get; set; }
        public int NumberOfLegs { get;  set; }
        public Arthropods() { }
        public Arthropods(string name, int age, int estimatedLifeTime, string area, bool canFly, int legs) : base(name, age, estimatedLifeTime, area)
        {
            this.Fly = canFly;
            this.NumberOfLegs = legs;
        }

        public override string Present()
        {
            return "I' m an arthropod with name:  " + Name + " and i used to live:  " + EstimatedLifeTime + " years and i live in:" + Area + " and let's see if a can fly: " + Fly + " and i have: " +
            NumberOfLegs + " legs and i don't have the vertebral column";
            

        }
        public override string HowYoungIsTheAnimal()
        {
            bool isAnimalYoung = false;
            if (EstimatedLifeTime - Age >= EstimatedLifeTime / 2 && EstimatedLifeTime - Age >= EstimatedLifeTime / 2 + 1)
            {
                isAnimalYoung = true;
            }

            if (isAnimalYoung)
            {
                return "This arthropod is an adult one, that can evolve.";
            }
            else
            {
                return "This arthropod is either too young or too old to evolve.";
            }
        }
    }
}
