﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Arthropods
{
    public class Myriapod : Arthropods
    {
        public Myriapod() { }
        public Myriapod(string name, int age, int estimatedLifeTime, string area, bool canFly, int legs)
            : base(name, age, estimatedLifeTime, "seas and oceans", false, legs)
        { }
        public override string Present()
        {
            return "I' m a myriapod with name:  " + Name + " and i used to live:  " + EstimatedLifeTime + " years and i live in:" + Area + " and let's see if a can fly: " + Fly + " and i have: " +
            NumberOfLegs + " legs and i d0n't have the vertebral column";

        }
    }
}
