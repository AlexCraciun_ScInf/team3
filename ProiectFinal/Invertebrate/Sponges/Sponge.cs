﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Sponges
{
    public class Sponges : Invertebrate
    {
        public Sponges() { }

        public Sponges(string name, int age, int estimatedLifeTime, string area)
            : base(name, age, estimatedLifeTime, area)
        {

        }

        public override string Present()
        {

            return "I'm a sponge with name:  " + Name + ", I have " + Age + " years, and i used to live:  " + EstimatedLifeTime +
                " years and i live in:" + Area + " and i don't have the vertebral column.";

        }
        public override string HowYoungIsTheAnimal()
        {
            bool isAnimalYoung = false;
            if (EstimatedLifeTime - Age >= EstimatedLifeTime / 2 && EstimatedLifeTime - Age >= EstimatedLifeTime / 2 + 1)
            {
                isAnimalYoung = true;
            }

            if (isAnimalYoung)
            {
                return "This arthropod is an adult one, that can evolve.";
            }
            else
            {
                return "This arthropod is either too young or too old to evolve.";
            }
        }
    }
}
