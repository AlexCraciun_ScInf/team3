﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Sponges
{
    public class SolitarySponge : Sponges
    {
        public SolitarySponge()
        { }

        public SolitarySponge(string name, int age, int estimatedLifeTime, string area)
            : base(name, age, estimatedLifeTime, area)
        { }

        public override string Present()
        {
            return base.Present()+
                ", and I live solitary.";
        }
    }
}
