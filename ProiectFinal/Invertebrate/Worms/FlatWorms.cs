﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Worms
{
    public class FlatWorms : Worms
    {
        public string WormColor { get; protected set; }
        public FlatWorms(string name, int age, int estimatedLifeTime, string area, string wormtype, string wormcolor) : base(name,age,estimatedLifeTime, area,wormtype)
        { 
            this.WormColor = wormcolor;
        }

        public override string Present()
        {
            return "I' m a flatworm with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " I am special because I am "+ WormColor;

        }

    }
}
