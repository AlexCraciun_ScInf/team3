﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Worms
{
    public class RoundWorms : Worms
    {
        public string WormPattern { get; protected set; }
        public RoundWorms(string name, int age, int estimatedLifeTime, string area, string wormtype, string wormpattern) : base(name, age, estimatedLifeTime, area, wormtype)
        {
            this.WormPattern = wormpattern;
        }

        public override string Present()
        {
            return "I' m a roundworm with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " I am special because I have " + WormPattern;

        }
    }
}
