﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Worms
{
    public abstract class Worms : Invertebrate
    {   public string WormType { get; protected set; } //  flatworms, roundworms and segmented worms.
        public Worms(string name, int age, int estimatedLifeTime, string area, string wormtype) : base(name, age, estimatedLifeTime, area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
            this.WormType = wormtype;
        }

        public override string Present()
        {
          
            return "I'm a worm with name:  " + Name + ", I have " + Age + " years, and i used to live:  " + EstimatedLifeTime + 
                " years and i live in:" + Area + " , my type is " + WormType + " and i don't have the vertebral column.";
        }

    }
}
