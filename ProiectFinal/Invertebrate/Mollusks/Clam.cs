﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Mollusks
{
    public class Clam : Mollusks
    { 
        public bool hasPearl { get; protected set; }
        public string PearlColor { get; protected set; }
        public Clam(string name, int age, int estimatedLifeTime, string area, int armsnumber, bool haspearl, string pearlcolor) :base(name, age, estimatedLifeTime, area, armsnumber)
            { this.hasPearl = haspearl;
              this.PearlColor = pearlcolor;
        }
        public override string Present()
        {
            return "I'm a clam with name:  " + Name + ", I have " + Age + " years, and i used to live:  " + EstimatedLifeTime +
                " years and i live in:" + Area + $" .I have got {ArmsNumber} arm" + $" and I{ (hasPearl ? "have got" : "haven't got")} a " + PearlColor + " pearl";

        }

    }
}

