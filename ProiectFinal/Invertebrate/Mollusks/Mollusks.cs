﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Mollusks
{
    public class Mollusks : Invertebrate
    {
        public int ArmsNumber { get; protected set; }
        public Mollusks(string name, int age, int estimatedLifeTime, string area, int armsnumber) : base(name, age, estimatedLifeTime, area)
        { 
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
            this.ArmsNumber= armsnumber;
        }
        public override string Present()
        {
          
            return "I'm a mollusk with name:  " + Name + ", I have " + Age + " years, and i used to live:  " + EstimatedLifeTime +
                " years and i live in:" + Area + $" .I have got {ArmsNumber} arm" ;
        }
    }

}
