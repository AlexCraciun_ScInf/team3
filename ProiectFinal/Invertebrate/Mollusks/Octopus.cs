﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Invertebrate.Mollusks
{
    public class Octopus : Mollusks
    {
        public bool hasInk { get; protected set; }
        public bool isSmart { get; protected set;}
        public Octopus(string name, int age, int estimatedLifeTime, string area, int armsnumber, bool hasink, bool issmart) : base(name, age, estimatedLifeTime, area, armsnumber)
        { this.hasInk = hasink;
          this.isSmart = issmart;
        }
        public override string Present()
        {

            return "I'm a octopus with name:  " + Name + ", I have " + Age + " years, and i used to live:  " + EstimatedLifeTime +
               " years and i live in:" + Area + $" .I have got {ArmsNumber} arm" + $", and I{ (hasInk ? "have got" : "haven't got")} a full tank of ink"
                 + $"I{(isSmart ? "am very" : "am not very ")}smart"; ;
        }
        }
    }

