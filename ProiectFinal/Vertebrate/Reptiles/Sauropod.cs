﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Reptiles
{

    public class Sauropod : Reptiles
    {
        public Sauropod() : base()
        {
            Legs = 4;
            Crawl = true;
        }

        public Sauropod(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area)
        {
            Legs = 4;
        }

        public override string Present()
        {
            return "I' m a lizard named " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
              " and i have the vertebral column." + "I have " + Legs + " legs.";
        }
    }
}
