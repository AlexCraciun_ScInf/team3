﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Reptiles
{
    public class Reptiles : Vertebrate
    {


        public Reptiles() : base()
        {
            Crawl = false;
        }
        public Reptiles(string name, int age, int estimatedLifeTime, string area) : base()
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;


        }
        public bool Crawl { get; protected set; }
    }
}
