﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Mammals
{
    public class Herbivore : Mammals
    {
        public Herbivore (string name, int age, int estimatedLifeTime, string area) :
                    base(name, age, estimatedLifeTime, area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
            Locomotion = "Climbing";
            Feet = 4;
            Fur = true;
        }

        public override string Present()
        {

            return "I' m a herbivore mammal with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " and my locomotion is:  " + Locomotion + " and i have : " + Feet + " feets and let's see if i have fur: " + Fur;
        }


    }
}