﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Mammals
{
    public class Mammals : Vertebrate
    {
        public string Locomotion { get; protected set; }

        public int Feet { get; protected set; }

        public bool Fur { get; protected set; }

        public Mammals()
        {

        }
        public Mammals(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area) { }
        public override string Present()
        {

            return "I' m a mammal with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " and my locomotion is:  " + Locomotion + " and i have : " + Feet +  " feets and let's see if i have fur: " + Fur;

        }




    }
}
