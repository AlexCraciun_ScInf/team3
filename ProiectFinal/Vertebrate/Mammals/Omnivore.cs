﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Mammals
{
    public class Omnivore : Mammals
    {
        public Omnivore()
        {

        }

        public Omnivore(string name, int age, int estimatedLifeTime, string area) :
            base(name, age, estimatedLifeTime, area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
            Locomotion = "Jumping";
            Feet = 4;
            Fur = true;
        }

        public override string Present()
        {

            return "I' m an omnivore mammal with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " and my locomotion is:  " + Locomotion + " and i have : " + Feet + " feets and let's see if i have fur: " + Fur;
        }
    }
}