﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate
{
    public class Birds : Vertebrate
    {
        // public bool FliesLongDistances { get; protected set; }

        public Birds(string name, int age, int estimatedLifeTime, string area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
        }


        public override string Present()
        {
            return "I' m a bird named " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
               " and i have the vertebral column";
        }

        public bool Sing { get; protected set; }


    }
}
