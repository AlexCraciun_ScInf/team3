﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Bird
{
    public class Penguins : Birds
    {
        public Penguins(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area)
        {
            Legs = 2;
            Fly = false;
            Sing = false;
        }

        public override string Present()
        {
            return "I' m a penguin" + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
               " and i have the vertebral column. I cannot sing.";
        }
    }
}
