﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Bird
{
    public class Parrots : Birds
    {
        public Parrots(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area)
        {
            Legs = 2;
            Fly = true;
            Sing = true;
        }

        public override string Present()
        {

            return "I' m a parrot " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
               " and i have the vertebral column. Sometimes I sing.";
        }
    }
}
