﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vertebrate.Fish
{
	public class Fish : Vertebrate
	{
		public bool Jaw { get;  protected set; }

		public bool Fins { get; protected set; }

		public bool Skeleton { get; protected set; }

		public Fish()
        {

        }
		public Fish(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area) { }
		public override string Present()
        {
			return "I' m a fish with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

			   + " and let's see if i have a jaw: " + Jaw + " and let's see if i have fins: " + Fins + " and let's see if i have a skeleton: " + Skeleton;

		}


	}
}
