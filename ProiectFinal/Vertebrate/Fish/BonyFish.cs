﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Vertebrate.Fish
{
    public class BonyFish : CartilaginousFish
    {
        //public string Subclass { get; protected set; }

        public BonyFish()
        {

        }

        public BonyFish (string name, int age, int estimatedLifeTime, string area)
            : base (name, age, estimatedLifeTime, area)
        {
            this.Name = name;
            this.Age = age;
            this.EstimatedLifeTime = estimatedLifeTime;
            this.Area = area;
            Jaw = true;
            Fins = false;
            Skeleton = true;
        }

        public override string Present()
        {
            return "I' m a BonyFish with the name: " + Name + " I usually live: " + EstimatedLifeTime + " years and i live in:" + Area

               + " and let's see if i have a jaw: " + Jaw + " and let's see if i have fins: " + Fins + " and let's see if i have a skeleton: " + Skeleton;

        }


    }
}