﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProiectFinalCore;
namespace Vertebrate
{
    
    public class Vertebrate : Animal
    {
        public Vertebrate() { }
        public Vertebrate(string name, int age, int estimatedLifeTime, string area) : base(name, age, estimatedLifeTime, area) { }
        public override string Present()
        {
            return "I' m a " + Name + " and i used to live " + EstimatedLifeTime + " years and i live in:" + Area +
                " and i have the vertebral column";

        }
        public bool Fly { get; protected set; }
        public int Legs { get; protected set; }
    }
}
